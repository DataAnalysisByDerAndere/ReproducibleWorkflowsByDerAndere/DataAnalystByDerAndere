// @title: ImageProcessingByDerAndere
// @author: DerAndere
// @created: 2018
// Copyright 2018 DerAndere
// @license: MIT
// @version: 0.0.1
// @language: ImageJ macro language
// @info: https://derandere.gitlab.io
// @description: ImageJ macros for image processing.

macro "Batch-AdustBCsetMaxValue" {
    // This macro processes all the images in a folder and any subfolders.
    extension = ".tif";
    extension2 = ".czi";
    inputDir = getDirectory("Choose Source Directory ");
    channel = getNumber("which channel should be adjusted?", 0);
    maxVal = getNumber("maxVal for the chosen channel?", 65535);

    //dir2 = getDirectory("Choose Destination Directory ");
    outputDir = inputDir + "BCSetMaxValueAdjusted\\" + "C" + channel + "\\";
    if (!File.exists(outputDir)) {
        File.makeDirectory(outputDir);
    }
    setBatchMode(true);
    n = 0;
    processFolder(inputDir);

    function processFolder(inputDir) {
        list = getFileList(inputDir);
        for (i = 0; i < list.length; i++) {
            if (endsWith(list[i], "/")) {
                processFolder(inputDir + list[i]);
            } 
            else if (endsWith(list[i], extension) || endsWith(list[i], extension2)) {
                processImage(inputDir, list[i]);
            }
        }
    }

    function processImage(inputDir, name) {
        img = inputDir + name;
        // only proceeds if file is there
        if (File.exists(img)) {
            // opens the images and gives them the temporary name "imgTemp"  
            open(img);
            ID = getImageID(img);
            selectImage(ID);
            dotIndex = indexOf(name, ".");
            basename = substring(name, 0, dotIndex);
            rename("imgTemp");
            // the next lines do your procedure
            Stack.setChannel(channel);
            stackSize = nSlices(imgTemp); // or nSlices(); or nSlices; ?
            for (z = 1; z <= stackSize; ++z) {
                setSlice(z) //?
                run("setMinAndMax", "min=0 max=maxVal");
                run("Apply LUT");
                wait(500);
                selectImage(ID);
                wait(500);
            }
            titleMod = "C" + channel + "adj";
            titleTitle = basename + titleMod + ".tif";
            wait(500);
            // add code here to analyze or process the image
            saveAs("tiff", outputDir + titleTitle);
            wait(500);
            close();
        }
    }
}

macro "SaveCurrentSliceAsTIF" {
    id = getImageID();
    title = getTitle(); // or getTitle;?
    dotIndex = indexOf(title, ".");
    basename = substring(title, 0, dotIndex);
    path = getDirectory("image");
    outputDir = inputDir + "Slices\\";
    if (!File.exists(outputDir)) {
        File.makeDirectory(outputDir);
    }
    currentSliceNumber = getSliceNumber();
    tileTitle = basename + "_S" + currentSliceNumber + "_mod1.tif";
    // using the ampersand allows spaces in the tileTitle to be handled correctly 
    run("Duplicate...", "title=&tileTitle");
    selectWindow(tileTitle);
    saveAs("tiff", outpurDir + tileTitle);
    close();
    selectImage(id);
}
// End of ImageProcessingByDerAndere
